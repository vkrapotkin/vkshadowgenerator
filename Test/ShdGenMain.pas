unit ShdGenMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.ImageList, FMX.ImgList, FMX.Objects, FMX.Controls.Presentation,
  FMX.StdCtrls, FMX.Effects, FMX.Filter.Effects, UVKShadowGenerator;

type



  TShdGenMainForm = class(TForm)
    img1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    gen:TVKShadowGenerator;

    procedure DrawRect(r:TRectF);
  public

  end;

var
  ShdGenMainForm: TShdGenMainForm;

implementation

uses
  system.math;

{$R *.fmx}

procedure TShdGenMainForm.drawRect(R:TRectF);
var
  bm:TBitmap;
  r1:TRectF;
begin
  gen.srcBitmap.SetSize(200,200);
  gen.srcBitmap.Clear($0);
  gen.srcBitmap.Canvas.BeginScene();
  gen.srcBitmap.Canvas.Fill.Color := TAlphaColorRec.white;
  gen.srcBitmap.Canvas.Fill.Kind := TBrushKind.Solid;
  gen.srcBitmap.Canvas.FillRect(R, 4, 4, AllCorners, 1);
  gen.srcBitmap.Canvas.EndScene;

  bm := TBitmap.Create;
  bm.SetSize(760, 380);
  bm.Canvas.BeginScene();
  bm.Canvas.Clear($ffaaaaaa);
  gen.CreateShadow(2);
  r1 := gen.resBitmap.BoundsF;
  bm.Canvas.DrawBitmap(gen.resBitmap, gen.resBitmap.BoundsF, r1, 1);
  bm.Canvas.EndScene;

  gen.CreateShadow(6);
  r1.Offset(175,0);
  bm.Canvas.BeginScene();
  bm.Canvas.DrawBitmap(gen.resBitmap, gen.resBitmap.BoundsF, r1, 1);
  bm.Canvas.EndScene;

  gen.CreateShadow(12);
  r1.Offset(175,0);
  bm.Canvas.BeginScene();
  bm.Canvas.DrawBitmap(gen.resBitmap, gen.resBitmap.BoundsF, r1, 1);
  bm.Canvas.EndScene;

  gen.CreateShadow(24);
  r1.Offset(175,0);
  bm.Canvas.BeginScene();
  bm.Canvas.DrawBitmap(gen.resBitmap, gen.resBitmap.BoundsF, r1, 1);
  bm.Canvas.EndScene;

  img1.Bitmap := bm;

  bm.Free;
end;

procedure TShdGenMainForm.FormCreate(Sender: TObject);
begin
  gen:=TVKShadowGenerator.Create;
  DrawRect(RectF(40,40,160,160));
end;

procedure TShdGenMainForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(gen);
end;



end.
