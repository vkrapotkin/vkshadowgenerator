unit UVKShadowGenerator;

interface
uses
  system.Types, system.sysutils, FMX.Graphics, FMX.Effects;

type
  TVKShadowGenerator = class
  private
    FShadowBitmap:TBitmap;
  public
    srcBitmap: TBitmap;
    resBitmap: TBitmap;
    shd: TShadowEffect;
    constructor Create;
    destructor Destroy; override;

    procedure CreateShadow(Distance:single);
  end;

implementation
{ TVKShadowGenerator }

constructor TVKShadowGenerator.Create;
begin
  shd := TShadowEffect.Create(NIL);
  FShadowBitmap := TBitmap.Create;
  srcBitmap := TBitmap.Create;
  resBitmap := TBitmap.Create;
end;

procedure TVKShadowGenerator.CreateShadow(Distance: single);
var
  shdOff:TPointF;
  r, shdRect, shdRectOff:TRectF;
  s,c:double; // sine, cosine
  d:Single;
begin
  FShadowBitmap.Clear(0);
  resBitmap.Clear(0);

  shd.Distance := Distance;
  shd.Direction := 90;

  // ���������� ����
  if Distance>24 then
    d := -0.1
  else
    d := Distance * (-0.1 / 24);
  shd.Opacity := 0.23 + d;

  // ���������� ����
  if Distance>24 then
    d := 0.4
  else
    d := Distance * (0.4 / 24);
  shd.Softness := 0.1 + d;

  // SineCosine(90deg)
  s := 1;
  c := 0;

  shdOff := Distance * PointF(c, s);
  shdRect := srcBitmap.BoundsF;
  shdRect.Offset(shdOff);
  shdRect.left := 0;

  shdOff := shd.GetOffset;
  shdOff.X := 0;
  shdOff.Y :=   shdOff.Y - 2;
  shdRectOff := srcBitmap.BoundsF;
  shdRectOff.Offset(shdOff);

  FShadowBitmap.SetSize(srcBitmap.Width, srcBitmap.Height);
  FShadowBitmap.CopyFromBitmap(srcBitmap);
  shd.ProcessEffect(NIL, FShadowBitmap, 0);

  resBitmap.SetSize(srcBitmap.Width, srcBitmap.Height);
  resBitmap.Canvas.BeginScene();
  resBitmap.Canvas.DrawBitmap(FShadowBitmap, srcBitmap.BoundsF, shdRectOff, 1);
  resBitmap.Canvas.DrawBitmap(FShadowBitmap, srcBitmap.BoundsF, shdRect, 1);
  resBitmap.Canvas.DrawBitmap(srcBitmap, srcBitmap.BoundsF, resBitmap.BoundsF, 1);
  resBitmap.Canvas.EndScene;
end;

destructor TVKShadowGenerator.Destroy;
begin
  FreeAndNil(Shd);
  FreeAndNil(FShadowBitmap);
  FreeAndNil(srcBitmap);
  FreeAndNil(resBitmap);
  inherited;
end;

end.
